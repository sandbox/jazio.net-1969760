#!/usr/bin/env drush
<?php
/* -----------------------------------------------------------------------------------
  Coriolanus, Drush Drupal Installer Script
  @author,credits Ovi Farcas, http://www.jazio.net 2013




  //------------------------------------------------------------------------------------ */
/* -----------------------------------------------------------------------------------

  .database settings

  //------------------------------------------------------------------------------------ */

// FILL THEM IN WITH YOUR MYSQL CREDENTIALS!
//$db_user = "FILL IN WITH YOUR MYSQL USERNAME";
//$db_pass = "FILL IN WITH YOUR MYSQL PASSWORD";
//Optional
$db_name = "coriolanus";

/* -----------------------------------------------------------------------------------

  .site settings

  //------------------------------------------------------------------------------------ */

$drupal_dir = "coriolanus";
$theme_dir = "coriolanus/sites/all/themes";
$makefile = "coriolanus.make";

$site_install_cmd = "drush site-install standard --account-name=%s --account-pass=%s --account-mail=%s --site-name=%s --db-url=mysql://%s:%s@localhost/%s -y";

$site_name = "Coriolanus";
$shortname = "awesome";

$account_name = "admin";
$account_pass = "dev";
$account_email = "webmaster@yourname.com";

/* -----------------------------------------------------------------------------------

  .verbose

  //------------------------------------------------------------------------------------ */

drush_print('------------------------------------------');
drush_print('CORIOLANUS, DRUPAL INSTALLER SCRIPT 0.1b');
drush_print('------------------------------------------');
drush_print("This script will install Drupal along with the most useful modules such as");
drush_print("Views, Features, Token, Webform, IMCE, WYSIWYG, Pathauto, Token, Devel, Modernizr and more");
drush_print("LAUNCH INITIATED");
drush_print("Please be patient a a few moments.");
drush_print('------------------------------------------');



/* -----------------------------------------------------------------------------------

  .drush make to download drupal + modules + themes + libraries

  //------------------------------------------------------------------------------------ */

$drush_make = "drush make -v $makefile $drupal_dir";

if (drush_shell_exec($drush_make)) {
    drush_print("Download Drupal, modules, themes, libraries, please be patient ...");
    drush_log('SUCCESS -- Drupal and modules downloaded', 'ok');
} else {
    drush_shell_exec_output();
    drush_log('FAIL -- drush make', 'error');
    exit(0);
}

/* -----------------------------------------------------------------------------------

  .install the fresh downloaded drupal

  //------------------------------------------------------------------------------------ */
$db_user = drush_get_option('db_user');
// If no password has been provided, prompt for one.
if (empty($db_user)) {
    $db_user = drush_prompt(dt('Mysql User'), NULL, TRUE, FALSE);
}
$db_pass = drush_get_option('db_password');
// If no password has been provided, prompt for one.


if (empty($db_pass)) {
    $db_pass = drush_prompt(dt('Mysql Password'), NULL, TRUE, TRUE);
}


if (drush_shell_cd_and_exec($drupal_dir, $site_install_cmd, $account_name, $account_pass, $account_email, $site_name, $db_user, $db_pass, $db_name, $site_name)) {
    drush_print('------------------------------------------');
    drush_print("Drupal installation initiated");
    drush_print("Your admin credentials will be: user: admin / password: dev");
    drush_log('SUCCESS -- install Drupal', 'ok');
} else {
    drush_shell_exec_output();
    drush_print('------------------------------------------');
    drush_log('RE -- install Drupal', 'error');
    exit(0);
}

/* -----------------------------------------------------------------------------------

  .enable wanted modules

  //------------------------------------------------------------------------------------ */

$modules_to_enable = "ctools views views_ui context context_ui features webform jquery_update libraries wysiwyg ckeditor";
$drush_en = "drush en $modules_to_enable -y";

if (drush_shell_cd_and_exec($drupal_dir, $drush_en)) {
    drush_shell_exec_output();
    drush_print('------------------------------------------');
    drush_log('SUCCESS -- modules enabling', 'ok');
} else {
    drush_shell_exec_output();
    drush_print('------------------------------------------');
    drush_log('FAIL -- modules enabling', 'error');
    exit(0);
}

/* -----------------------------------------------------------------------------------

  .disable unwanted core modules

  //------------------------------------------------------------------------------------ */
$modules_to_disable = "update";
$drush_dis = "drush dis $modules_to_disable -y";

if (drush_shell_cd_and_exec($drupal_dir, $drush_dis)) {
    drush_shell_exec_output();
    drush_print('------------------------------------------');
    drush_log('SUCCESS -- unwanted core modules', 'ok');
} else {
    drush_shell_exec_output();
    drush_print('------------------------------------------');
    drush_log('FAIL -- unwanted core module', 'error');
    exit(0);
}


/* -----------------------------------------------------------------------------------

  .cron + clean

  //------------------------------------------------------------------------------------ */
if (drush_shell_cd_and_exec($drupal_dir, 'drush cron -y')) {
    drush_print('------------------------------------------');
    drush_log('SUCCESS -- cron run', 'ok');
} else {
    drush_shell_exec_output();
    drush_print('------------------------------------------');
    drush_log('FAIL -- cron run', 'error');
    exit(0);
}


if (drush_shell_cd_and_exec($drupal_dir, 'drush cc all -y')) {
    drush_print('------------------------------------------');
    drush_log('SUCCESS -- cache clear', 'ok');
} else {
    drush_shell_exec_output();
    drush_print('------------------------------------------');
    drush_log('FAIL -- cache clear', 'error');
    exit(0);
}